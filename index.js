// Mock database
let posts = [];
// Movie post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// Prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Movie Post successfully added');
})

// Show posts

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;
}

// Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	/*
		i = 1, posts.length = 2
		posts[0].id = 1 === '#txt-edit-id' = 2
		posts[1].id = 2 === '#txt-edit-id' = 2
	*/

	for(let i = 0; i < posts.length; i++){

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated');

			break;
		}
	}
})

// Delete post

const deletePost = (id) => {
	for(let i = 0; i < posts.length; i++){
		let postId = (id === posts[i].id.toString())
		if(postId){

			const postElem = document.querySelector(`#post-${id}`)
			// posts[i].title = document.querySelector('#txt-edit-title').value;
			// posts[i].body = document.querySelector('#txt-edit-body').value;
			postElem.remove();
			posts.splice(i,1);

			showPosts(posts);
			alert('Successfully deleted');

			break;
		}
	}
}







